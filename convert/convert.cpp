#include <iostream>
#include <iomanip>
#include <filesystem>
#include <glib.h>
#include <glib/gi18n.h>

#include "convert.hpp"

using namespace std ;

const int Convert::blocksize = 32 ;
unsigned int Convert::resourcecount =0 ;

bool Convert::SetOutputFolder(std::string folder) {
    GDateTime* now = g_date_time_new_now_utc () ;
    gchar *temp = g_date_time_format(now,"%F %T") ;
    timestamp = strdup( temp );
    g_free(temp) ;

    error_code ec ;
    filesystem::directory_entry de(folder) ;

    if (de.is_directory(ec)) {
        output_folder = folder ;
        return true ;
    }
    
    if (filesystem::create_directory(folder,ec)) {
        cout << folder << " created" << endl ;
        output_folder = folder ;
        return true ;
    }
    cerr << "Error " << ec.value() << " creating " << folder << endl ;
    return false ;
}

void Convert::SetEncrypt() {
    xorkey = "" ;
    while (xorkey.length() < blocksize) {
        xorkey.append(timestamp) ;
    }
    cout << "xorkey set to " << xorkey << endl ;
}
void Convert::All(const Resources& res) {

    
    error_code ec ;
    filesystem::directory_entry de(output_folder) ;
    if (!de.is_directory(ec)) {
        cerr << "No output folder specified. Aborting..." << endl ;
        return ;
    }

    AllBegin() ;
    Resources::const_iterator resptr ;
    for (resptr = res.begin(); resptr != res.end(); ++resptr) {
        Resource(*resptr) ;
    }
    AllEnd() ;
}

void Convert::Resource(const ResourceConfig& res) 
{
    resourceid = ++resourcecount ;
    config = res ;
    hash = "" ;
    ifstream ifile( config.filename , ios::binary) ;
    if (!ifile.is_open()) {
        cout << "Error opening " << config.filename << endl ;
        return ;
    } else {
        cout << "Opened " << config.filename << endl ;
    }
    emittedcount = 0;
    GChecksum *checksum=NULL ;
    checksum = g_checksum_new (G_CHECKSUM_MD5);

    ResBegin() ;
    char buffer[blocksize] ;
    while (!ifile.eof()) {
        ifile.read(buffer,blocksize) ;
        if (ifile.gcount() > 0) {
            g_checksum_update(checksum , (const guchar *)buffer, ifile.gcount()) ;
            if (xorkey.length() > 0) {
                for (int i=0; i < ifile.gcount(); i++) {
                    buffer[i] ^= xorkey.c_str()[i] ;
                }
            }
            ResEmit(buffer, ifile.gcount() ) ;
            emittedcount += ifile.gcount() ;

        }
    }
    ifile.close();
    hash = g_checksum_get_string(checksum) ;

    ResEnd() ;
}

void Convert::AllBegin() {
    cerr << "Begin Conversion of resources" << endl ;
}


void Convert::ResBegin() {
    cout << "Name: " << config.name << " Id : " << resourceid << endl ;
}

void Convert::ResEmit(char *ptr, int size) {
    cout << "\t" ;
    for (int i=0; i<size; i++) {
        cout << setw(2) << setfill('0') << hex << (int)ptr[i] ;
        //cout << ptr[i] ;
    }
    cout << endl ;
}

void Convert::ResEnd() {
    cout << dec << "End of File : " << config.filename << " Size "  << emittedcount << " MD5 checksum " << hash << endl ;
}

void Convert::AllEnd() {
    cout << "Completed converting " << resourcecount << " resources" << endl ;
}