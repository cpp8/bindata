#ifndef __CONVERTCPP_HPP__
#define __CONVERTCPP_HPP__
#include "convert.hpp"
class ConvertCPP : public Convert {
public:
private:
    std::ofstream hfile ;    
    std::ofstream cfile ;
    void virtual AllBegin() ;
    void virtual ResBegin() ;
    void virtual ResEmit(char *ptr, int size) ;
    void virtual ResEnd() ; 
    void virtual AllEnd() ;
};

#endif