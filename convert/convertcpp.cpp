#include <iostream>
#include <fstream>
#include <filesystem>
#include <iomanip>

#include "convertcpp.hpp"

using namespace std ;
void ConvertCPP::AllBegin() {
    string hfilename = filesystem::path(output_folder) / "resources.h" ;
    hfile.open(hfilename , ios::out) ;
    if (!hfile) {
        cerr << "Error creating the header file in " << output_folder << endl ;
        return ;
    }

    hfile << "// Name: resources.h" << endl ;
    hfile << "// Created: " << timestamp << endl << endl ;
    hfile << "#define CREATION_TIMESTAMP " << '"' << timestamp << '"' << endl ;
    hfile << "#define BLOCK_SIZE " << blocksize << endl ;
}

void  ConvertCPP::ResBegin() {
    cout << "Resource " << config.name << " begin" << endl ;
    hfile << "// Resource " << config.name << endl ;
    hfile << "#define ID_" << config.name << " " << resourceid << endl ;
    string basefilename = config.name + "_resource.c" ;
    string cfilename = filesystem::path(output_folder) / basefilename ;
    cfile.open(cfilename , ios::out) ;
    if (!cfile) {
        cerr << "Error creating the resource file in " << output_folder << endl ;
        return ;
    }

    cfile << "\tchar DATA_" << config.name << "[] =" << endl ;
}

void  ConvertCPP::ResEmit(char* ptr, int size) {
    cfile << "\t" << '"' ;
    for (int i=0; i<size; i++) {
        cfile << setw(2) << setfill('0') << hex << (int)ptr[i] ;
        //cout << ptr[i] ;
    }
    cfile << '"' << endl ;
}

void  ConvertCPP::ResEnd() {
    cfile << "\t;" << endl ;
    cfile.close() ;

    hfile << "#define FILENAME_" << config.name << " " << filesystem::path(config.filename).filename() << endl ;
    hfile << "#define FILESIZE_" << config.name << " " << dec << emittedcount << endl ;
    hfile << "#define MD5_HASH_" << config.name << " " << '"' << hash << '"' << endl ;
    hfile << "#include " << '"' << config.name << "_resource.c" << '"' << endl ;
    hfile << endl ;

}

void  ConvertCPP::AllEnd() {
    hfile.close() ;
}