#include <iostream>
#include "convert.hpp"
#include "convertcpp.hpp"
#include "convertada.hpp"

using namespace std ;
void basic() {
    ResourceConfig cfg1 ;
    cfg1.name = "CONVERTHEADER" ;
    cfg1.filename = "convert.hpp" ;
    Convert  conv1 ;
    conv1.Resource(cfg1);

    ResourceConfig cfg2 ;
    cfg2.name = "CONVERTSRC" ;
    cfg2.filename = "convert.cpp" ;
    cfg2.Show();
    Convert conv2 ;
    conv2.Resource(cfg2);
}

int main(int argc, char **argv) {
    cout << argc << " " << argv[0] << endl ;
    Config cfg("test.json") ;
    const Resources& resources = cfg.Get() ;

    ConvertCPP converter ;
    converter.SetOutputFolder("output") ;
    converter.All( resources );

    ConvertAda converter2 ;
    converter2.SetOutputFolder("output") ;
    converter2.All( resources );

    ConvertCPP encconverter ;
    encconverter.SetOutputFolder("encoutput") ;
    encconverter.SetEncrypt() ;
    encconverter.All(resources) ;

    ConvertAda encconverter2 ;
    encconverter2.SetOutputFolder("encoutput") ;
    encconverter2.SetEncrypt() ;
    encconverter2.All(resources) ;
}