#ifndef __CONVERTADA_HPP__
#define __CONVERTADA_HPP__
#include "convert.hpp"
class ConvertAda : public Convert {
public:
private:
    std::ofstream specfile ;    

    void virtual AllBegin() ;
    void virtual ResBegin() ;
    void virtual ResEmit(char *ptr, int size) ;
    void virtual ResEnd() ; 
    void virtual AllEnd() ;
};

#endif