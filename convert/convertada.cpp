#include <iostream>
#include <fstream>
#include <filesystem>
#include <iomanip>

#include "convertada.hpp"

using namespace std ;

void ConvertAda::AllBegin() {

    string specfilename = filesystem::path(output_folder) / "resources.ads" ;
    specfile.open(specfilename , ios::out) ;
    if (!specfile) {
        cerr << "Error creating the header file in " << output_folder << endl ;
        return ;
    }

    specfile << "-- Package : resources " << endl ;
    specfile << "-- Created : " << timestamp << endl ;
    specfile << "package resources is" << endl ; 
    specfile << endl << "    CREATION_TIMESTAMP" << " : constant String := " << '"' << timestamp << '"' << " ;" << endl ;
    specfile <<         "    BLOCK_SIZE : constant := " << blocksize << " ;" << endl ;
}

void  ConvertAda::ResBegin() {
    cout << "Resource " << config.name << " begin" << endl ;

    specfile << endl ;
    specfile << "-- Resource " << config.name << endl ;

    specfile << "    ID_" << config.name << " : constant := " << resourceid << " ;" << endl ;
    specfile << "    DATA_" << config.name << " : constant String := " << endl ;
}

void  ConvertAda::ResEmit(char* ptr, int size) {
    if (emittedcount > 0) {
        specfile << "    & " ;
    } else {
        specfile << "       " ;
    }
    specfile << '"' ;
    for (int i=0; i<size; i++) {
        specfile << setw(2) << setfill('0') << hex << (int)ptr[i] ;
        //cout << ptr[i] ;
    }
    specfile << '"' << endl ;
}

void  ConvertAda::ResEnd() {

    specfile << "    ;" << endl ;
    specfile << "    FILENAME_" << config.name << " : constant String := " << filesystem::path(config.filename).filename() << " ;" << endl ;
    specfile << "    FILESIZE_" << config.name << " : constant := " << dec << emittedcount << " ;" << endl ;
    specfile << "    MD5_HASH_" << config.name << " : constant String := " << '"' << hash << '"' << " ;" << endl ;
    specfile << endl ;

}

void  ConvertAda::AllEnd() {
    specfile << "end resources ;" << endl ;
    specfile.close() ;
}