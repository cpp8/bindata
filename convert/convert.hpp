#ifndef __CONVERT_HPP__
#define __CONVERT_HPP__

#include <fstream>
#include <vector>
#include "../config/config.hpp"

class Convert {
public:
   bool SetOutputFolder(const std::string folder) ;
   void SetEncrypt() ;
   void All(const Resources& res) ;
   void Resource(const ResourceConfig& res) ;
protected:
   static const int blocksize ;
   std::string output_folder ;
   char *timestamp ;
   std::string xorkey ;

   unsigned int emittedcount ;
   std::string hash ;
   ResourceConfig config ;
   unsigned int resourceid ;


   void virtual AllBegin() ;
   void virtual ResBegin() ;
   void virtual ResEmit(char *ptr, int size) ;
   void virtual ResEnd() ;
   void virtual AllEnd() ;
private:
   static unsigned int resourcecount ;
} ;


#endif
