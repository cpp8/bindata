CXX=clang
EXEC := resource
SOURCES = $(wildcard *.cpp)
HEADERS = $(wildcard *.hpp) 
OBJECTS = $(SOURCES:.cpp=.o)

OBJLIB = ./lib/libbindata.a 

CXXFLAGS := -g -I./options -I./config -I./convert -std=gnu++17 -Wall -Wextra $(shell pkg-config --cflags gtk+-3.0)
LDFLAGS := $(OBJLIB) -lstdc++ -lstdc++fs  -ljansson $(shell pkg-config --libs gtk+-3.0)


all: $(OBJECTS) $(HEADERS) $(OBJLIB)
	$(CXX) $(OBJECTS) $(CXXFLAGS) $(LDFLAGS) -o $(EXEC)

clean:
	ls $(SOURCES)
	ls $(OBJECTS)
	rm -f $(EXEC) $(OBJECTS)
	rm -f $(OBJLIB)
	$(MAKE) -C options clean
	$(MAKE) -C config clean
	$(MAKE) -C convert clean

%.o:%.cpp
	$(CXX) $(CXXFLAGS) -c $(<)

$(OBJLIB) :
	$(MAKE) -C options 
	$(MAKE) -C config
	$(MAKE) -C convert