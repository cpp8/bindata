with Text_Io; use Text_Io ;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with Ada.Streams ; use Ada.Streams ;
with Interfaces ; use Interfaces ;
with GNAT.MD5 ;

package body resource is
   Nibble_Hex : constant String := "0123456789ABCDEF";

   function Value (Hex : Character) return Interfaces.Unsigned_8 is
   begin
      if Hex in '0' .. '9' then
         return Interfaces.Unsigned_8
             (Character'Pos (Hex) - Character'Pos ('0'));
      end if;
      if Hex in 'a' .. 'f' then
         return Interfaces.Unsigned_8
             (10 + Character'Pos (Hex) - Character'Pos ('a'));
      end if;
      if Hex in 'A' .. 'F' then
         return Interfaces.Unsigned_8
             (10 + Character'Pos (Hex) - Character'Pos ('A'));
      end if;
      raise Program_Error with "InvalidHex";
   end Value;

   function digest( block : block_ptr_type ) return String is
      buffer : ada.Streams.Stream_Element_Array(1..Stream_Element_Offset(block.all'Length) ) ;
      for buffer'Address use block.all'Address ;
       C : Gnat.MD5.Context := Gnat.Md5.Initial_Context ;
   begin
      GNAT.MD5.Update(C,buffer) ;
      return Gnat.MD5.Digest(C) ;
   end digest ;


   function Retrieve(id : integer ;
                       filename : string ;
                       filesize : integer ;
                       hash : string ;
                       data : string ;
                       decrypt : boolean := false ;
                       blocklen : integer := 0 ;
                       timestamp : string := "" ) return block_ptr_type is
      block : block_ptr_type ;
      H : Integer ;
      Upper, Lower : Interfaces.Unsigned_8 ;
      deckey : String(1..blocklen) ;
      procedure fillup_decrypt_key is
         tsptr : integer := 1 ;
      begin
         for keyptr in deckey'range
         loop
            deckey(keyptr) := timestamp(tsptr) ;
            tsptr := tsptr + 1 ;
            if tsptr > timestamp'length
            then
               tsptr := 1 ;
            end if ;
         end loop ;
      end fillup_decrypt_key ;

      procedure decrypt_block is
         keybytes : block_type(1..blocklen) ;
         for keybytes'address use deckey'address ;
         keybytesptr : integer := 1 ;
      begin
         for blkptr in block.all'range
         loop
            block(blkptr) := block(blkptr) xor keybytes(keybytesptr) ;
            keybytesptr := keybytesptr + 1 ;
            if keybytesptr > keybytes'length
            then
               keybytesptr := 1 ;
            end if ;
         end loop ;
      end decrypt_block ;

   begin
      if decrypt then
         fillup_decrypt_key ;
         put("Decryption Key Set To ");
         put_line(deckey) ;
      end if ;

      if filesize /= data'length/2 then
         Put_Line("File size and Data Size do not match");
         Put("Expected "); Put(filesize); Put(" Got "); Put(data'length) ; New_line ;
      end if ;
      block := new block_type(1..filesize);
      for C in 1 .. filesize loop
         H := C*2 - 1 ;
         Upper := Value(data(H)) ;
         Lower := Value(data(H+1)) ;
         block(C) := Upper * 16 + Lower;
      end loop ;

      if decrypt
      then
         decrypt_block ;
      end if ;

      declare
         dig : String := digest(block) ;
      begin
         Put_Line("Digest of data");
         Put("Expected : ") ; Put_Line(hash) ;
         Put("Got      : ") ; Put_Line(dig) ;
         if hash /= dig then
            Put_Line("Hash values did not match");
            return null ;
         end if ;
      end ;
      return block ;
   end Retrieve ;

end resource ;
