With Text_Io ; use Text_Io ;
with resource ;

with resources ;

procedure Main is
   result : resource.block_ptr_type ;
begin
   --  Insert code here.
   result := resource.Retrieve(resources.ID_HEADER1,
                     resources.FILENAME_HEADER1,
                     resources.FILESIZE_HEADER1,
                     resources.MD5_HASH_HEADER1,
                               resources.DATA_HEADER1,
                               true ,
                               resources.BLOCK_SIZE ,
                              resources.CREATION_TIMESTAMP ) ;
   declare
      header : string(1..resources.FILESIZE_HEADER1) ;
      for header'Address use result.all'Address ;
   begin
      Put_Line(header);
   end ;

end Main;
