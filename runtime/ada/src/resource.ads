with system ;
with Interfaces ;

package resource is
    type block_type is array (Integer range <>) of Interfaces.Unsigned_8;
    type block_ptr_type is access block_type ;
    function Retrieve(id : integer ;
                       filename : string ;
                       filesize : integer ;
                       hash : string ;
                       data : string) return block_ptr_type ;

end resource ;
