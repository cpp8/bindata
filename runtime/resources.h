// Name: resources.h
// Created: 2020-09-06 09:16:21

#define CREATION_TIMESTAMP "2020-09-06 09:16:21"
#define BLOCK_SIZE 32
// Resource HEADER1
#define ID_HEADER1 5
#define FILENAME_HEADER1 "convert.hpp"
#define FILESIZE_HEADER1 751
#define MD5_HASH_HEADER1 "0352385e812792a2624c087c969975d0"
#include "HEADER1_resource.c"

// Resource HEADER2
#define ID_HEADER2 6
#define FILENAME_HEADER2 "convertcpp.hpp"
#define FILESIZE_HEADER2 361
#define MD5_HASH_HEADER2 "0032cbce7c374cc681a96b4c10678162"
#include "HEADER2_resource.c"

