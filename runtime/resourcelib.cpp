#include <iostream>
#include <glib.h>
#include <glib/gi18n.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

#include "resourcelib.hpp"
using namespace std ;

byte* Retrieve(int resourceid, char* filename, int filesize , char* filehash, char* data, bool decrypt, int blocksize,const char *timestamp) 
{
    char *decryptkey = NULL ;
    int keysize ;
    if (decrypt) {
        decryptkey = (char *)malloc(blocksize+1);
        keysize=0;
        while (keysize < blocksize) {
            strcpy(&decryptkey[keysize], timestamp) ;
            keysize += strlen(timestamp) ;
        }
        printf("Decryption key set to : %s\n" , decryptkey) ;
    }

    byte *buffer ;
    buffer = new byte[filesize] ;
    if (buffer == NULL) return buffer ;

    GChecksum *checksum=NULL ;
 
    unsigned long byteval ;
    char nexthex[3] ;
    nexthex[2] = 0 ;
    int decptr = 0 ;
    for (int ptr=0; ptr<filesize; ptr++) {
        nexthex[0] = *data++ ;
        nexthex[1] = *data++ ;
        byteval = strtoul(nexthex,NULL,16) ;
        buffer[ptr] = (byte)byteval ;
        if (decrypt) {
            buffer[ptr] = byte((char)buffer[ptr] ^ decryptkey[decptr++]) ;
            if (decptr >= blocksize) decptr = 0 ;
        }
    }

    checksum = g_checksum_new (G_CHECKSUM_MD5);
    g_checksum_update(checksum , (const guchar *)buffer , filesize) ;
    const gchar *hashgot = g_checksum_get_string(checksum) ;

    if (strcmp(hashgot,filehash) != 0) {
        cout << "Hash Error" << endl ;
        cout << "Expecting : " << filehash << endl ;
        cout << "Got       : " << hashgot << endl ;
    }

    return buffer ;
}
