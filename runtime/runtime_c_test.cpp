#include <iostream>
#include "resourcelib.hpp"

#include "resources.h"

using namespace std ;

int main(int argc, char **argv) {
    std::byte *data ;
    data = Retrieve(ID_HEADER1, FILENAME_HEADER1 , FILESIZE_HEADER1 , MD5_HASH_HEADER1 , DATA_HEADER1 , true , BLOCK_SIZE , CREATION_TIMESTAMP) ;
    if (data == NULL) {
        cout << "Failed to retrieve HEADER1" << endl ;
    } else {
        cout << (char *)data << endl ;
    }

    data = Retrieve(ID_HEADER2, FILENAME_HEADER2 , FILESIZE_HEADER2 , MD5_HASH_HEADER2 , DATA_HEADER2 , true , BLOCK_SIZE , CREATION_TIMESTAMP) ;
    if (data == NULL) {
        cout << "Failed to retrieve HEADER1" << endl ;
    } else {
        cout << (char *)data << endl ;
    }
}