#include <cstdlib>

#include <getopt.h>
#include <iostream>
#include "options.hpp"

using namespace std ;

Options::Options(int _argc, char **_argv) 
: argc(_argc) , argv(_argv) , argptr(0)
{
    if (argc <= 1) {
        Help() ;
        exit(EXIT_FAILURE) ;
    }
    verbose = false ;
    output_folder = "" ;
    language = "C" ;
    encrypt = false ;
}

void Options::Help() {
    cerr << "resource - Version 0.2 - " << __TIMESTAMP__ << endl ;
    cerr << "usage: resource <options> <config>" << endl ;
    cerr << "\t -v --verbose \t Verbose" << endl ;
    cerr << "\t -h --help\t Help" << endl ;
    cerr << "\t -e --encrypt\t" << endl ;
    cerr << "\t -l --language\t C|Ada" << endl ;
    cerr << "\t -o --output\t <folder name>" << endl ;
    cerr << "\t <config>\t config in json" << endl ;
    exit(EXIT_FAILURE);
}

void Options::Show() {
    cerr << "Options playback " << endl ;
    cerr << "Language " << language << endl ;
    cerr << "Output folder " << output_folder << endl ;
    if (encrypt) {
        cerr << "Data will be encrypted " << endl ;
    } else {
        cerr << "Data will be in plaintext" << endl ;
    } 
}

int Options::ArgPtr() {
    int opt ;
    int idxptr = 0 ;

    if (optind > 1) {
        return optind ;
    }

    struct option options [] = {
        {"output" , required_argument , 0 , 'o' } ,
        {"language" , required_argument, 0 , 'l'} ,
        {"encrypt" , 0 , 0 , 'e'} ,
        {0 , 0, 0, 0}
    } ;

    while ((opt = getopt_long(argc, argv, "hel:o:v" , options, &idxptr )) != -1) {
        switch (opt) {
        case 'h':
            Help() ;
            break ;
        case 'v':
            verbose = true ;
            cerr << "Verbose" << endl ;
            continue ;
        case 'e':
            encrypt=true;
            continue ;
        case 'l':
            language = optarg ;
            if ((language.compare("C") == 0) || (language.compare("Ada") == 0)) {
                continue ;
            } else {
                cerr << language << " is not a supported language" << endl ;
                exit(EXIT_FAILURE);
            }
        case 'o':
            output_folder = optarg ;
            continue ;
        default: 
            Help();
            break;
        }
    }

    if (verbose) {
        Show() ;
    }
    return optind ;    
}