#ifndef _OPTIONS_HPP_
#define _OPTIONS_HPP_
#include <string>

class Options {
    public:
        Options(int argc, char **argv) ;
        bool verbose;
        bool encrypt ;
        std::string output_folder; 
        std::string language ;
        
        void Help() ;
        void Show() ;
        int ArgPtr() ;

    private:
        int argptr ;
        int argc;
        char **argv;
        Options() ;
} ;

#endif