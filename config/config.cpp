#include <iostream>
#include <fstream>

#include <jansson.h>

#include "config.hpp"

using namespace std ;

void ResourceConfig::Show() const {
    cout << "Name : " << name ; 
    cout << "Filename : " << filename ;
    cout << endl ;
}

Config::Config(std::string _filename) {
    cout << "Loading config from " << _filename << endl ;
    json_t *json;
    json_error_t error;

    json = json_load_file(_filename.c_str() , 0 , &error);
    if(!json) {
        /* the error variable contains error information */
        cout << "Error opening " << _filename << endl ;
        cout << error.text << endl ;
        return;
    }


    json = json_object_get(json,"resources") ;
    if (!json) {
        cout << "Expecting the resource list specification" << endl ;
        return ;
    }
    cout << "Top level. type is " << json_typeof(json) << " Array ? " << json_is_array(json) << endl;
    unsigned int numres = json_array_size(json) ;

    json_t *value ;
    for (unsigned int i=0; i<numres; i++)
    {
        value = json_array_get(json,i);
        if (!value) break ;
        json_t *key, *val ;

        key = json_object_get( value , "name") ;
        val = json_object_get( value , "filename") ;
        cout << "Name " << json_string_value(key) << " Filename " << json_string_value(val) << endl ;
        ResourceConfig cfg ;
        cfg.name = json_string_value(key) ;
        cfg.filename = json_string_value(val) ;
        resources.push_back(cfg);
    }
}

const Resources& Config::Get() const {
    cout << "Returning " << endl;
    return resources ;
}

void Config::Show() const {
    list<ResourceConfig>::const_iterator resptr ;
    for (resptr = resources.begin(); resptr != resources.end(); ++resptr) {
        resptr->Show();
    }
}