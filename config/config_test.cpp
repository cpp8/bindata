#include "config.hpp"

int main(int argc, char **argv) {
    Config cfg("../etc/config.json") ;
    const Resources& resources = cfg.Get() ;
    cfg.Show();
}