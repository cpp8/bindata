#ifndef __CONFIG_HPP__
#define __CONFIG_HPP__
#include <string>
#include <list>

class ResourceConfig {
public:
    std::string name ;
    std::string filename ;
    void Show() const ;
} ;

typedef std::list<ResourceConfig> Resources ;

class Config {
public:
    Config(std::string _filename) ;
    const Resources& Get() const ;
    void Show() const ;
private:
    Resources resources ;
} ;

#endif
