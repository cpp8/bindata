#include <iostream>
#include <string>
#include "options/options.hpp"
#include "config/config.hpp"
#include "convert/convertcpp.hpp"
#include "convert/convertada.hpp"

using namespace std ;
int main(int argc, char **argv) {
    Options options(argc,argv) ;
    string configfilename = argv[options.ArgPtr()] ;
    cout << "Configuration file " << configfilename << endl ;
    Config cfg(configfilename) ;
    if (options.language.compare("C") == 0) {
        ConvertCPP convert;
        convert.SetOutputFolder( options.output_folder ) ;
        if (options.encrypt) convert.SetEncrypt() ;
        convert.All( cfg.Get() );
    }
    if (options.language.compare("Ada") == 0) {
        ConvertAda convert;
        convert.SetOutputFolder( options.output_folder ) ;
        if (options.encrypt) convert.SetEncrypt() ;
        convert.All( cfg.Get() );
    }
}